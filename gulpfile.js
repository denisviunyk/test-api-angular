var gulp = require("gulp"),
    yamlinc = require("gulp-yaml-include"),
    clipboard = require("gulp-clipboard"),
    colors = require('colors'),
    bump = require('gulp-bump'),
    exec = require('child_process').exec,
    SwaggerParser = require('swagger-parser'),
    tagVersion = require('gulp-tag-version');
 
gulp.task("yamlconcat", function () {
    return gulp.src("./src/*.yml")
        .pipe(yamlinc())
        .pipe(gulp.dest("./dist/"));
});

gulp.task('bump', function(){
    gulp.src('./package.json')
    .pipe(bump())
    .pipe(gulp.dest('./'));

    gulp.src('./src/api.yml')
    .pipe(bump())
    .pipe(gulp.dest('./src'));
  });

gulp.task('clipcopy', function(){
    gulp.src("./dist/api.yml").pipe(clipboard());
    console.log(colors.green("✅  ✅  ✅  😅  DONE, API is on your clipboard!"));
});

gulp.task('tag', function() {
    return gulp.src(['./package.json']).pipe(tagVersion());
});

gulp.task('generateAngularCode', function() {
    exec('java -jar openapi-generator-cli.jar generate -g typescript-angular -i ./dist/api.yml -o ./dist', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
    });
});

gulp.task('validate', function() {
    SwaggerParser.validate('dist/api.yml', function(err, api) {
    if (err) {
        console.error(err);
    }
    else {
        console.log(colors.green("API OK - Name: %s, Version: %s"), api.info.title, api.info.version);
    }
    });
});

gulp.task('watch', function() {
    gulp.watch('src/**/*.yml', ['yamlconcat', 'validate']);
});

gulp.task('release', ['bump', 'yamlconcat', 'clipcopy', 'generateAngularCode']);
