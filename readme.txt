# To develop the API
* Add changes to src folder

# To validate your code
* yarn validate

# To get the final API yml
* yarn build

# To release a version
* yarn release
