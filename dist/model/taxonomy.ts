/**
 * Test api
 * Test api for example.
 *
 * OpenAPI spec version: 1.0.2
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


/**
 * Taxonomy term
 */
export interface Taxonomy { 
    id?: string;
    name: string;
    vocabulary?: Taxonomy.VocabularyEnum;
}
export namespace Taxonomy {
    export type VocabularyEnum = 'article_type' | 'tag';
    export const VocabularyEnum = {
        ArticleType: 'article_type' as VocabularyEnum,
        Tag: 'tag' as VocabularyEnum
    };
}
